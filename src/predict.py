from train import Dataset
import torch

def predict(model, test_data):

    test = Dataset(test_data)
    test_dataloader = torch.utils.data.DataLoader(test, batch_size=2)

    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")

    predicted_label = []
    actual_label = []
    with torch.no_grad():
        for test_input, test_label in (test_dataloader):
            labels = test_label.to(device)
            attention_mask = test_input['attention_mask'].to(device)
            input_ids = test_input['input_ids'].squeeze(1).to(device)

            logits = model(input_ids, attention_mask)
            
            probs = F.softmax(logits, dim=1)
            output = torch.argmax(probs, dim=1)
            
            predicted_label += output
            actual_label += labels
            
    return predicted_label, actual_label